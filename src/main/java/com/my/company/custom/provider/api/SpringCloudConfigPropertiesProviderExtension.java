package com.my.company.custom.provider.api;

import static org.mule.runtime.api.component.ComponentIdentifier.builder;

import org.mule.runtime.api.component.ComponentIdentifier;
import org.mule.runtime.extension.api.annotation.Configurations;
import org.mule.runtime.extension.api.annotation.Export;
import org.mule.runtime.extension.api.annotation.Extension;
import org.mule.runtime.extension.api.annotation.dsl.xml.Xml;



/**
* This is the main class of an extension, is the entry point from which configurations, connection providers, operations
* and sources are going to be declared.
*/
@Xml(prefix = "spring-cloud-config-provider")
@Extension(name = "spring-cloud-config-provider")
@Configurations(SpringCloudConfigClientConfiguration.class)
@Export(classes = CustomConfigurationPropertiesProviderFactory.class,
resources = "META-INF/services/org.mule.runtime.config.api.dsl.model.properties.ConfigurationPropertiesProviderFactory")

public class SpringCloudConfigPropertiesProviderExtension {
	
	public static final String EXTENSION_NAMESPACE = "spring-cloud-config-provider";
	public static final String CONFIG_ELEMENT = "config";
	public static final ComponentIdentifier SPRING_CLOUD_CONFIG_PROPERTIES_PROVIDER = builder().namespace(EXTENSION_NAMESPACE).name(CONFIG_ELEMENT).build();

}
