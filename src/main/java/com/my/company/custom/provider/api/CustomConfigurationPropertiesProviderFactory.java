/*
 * (c) 2003-2018 MuleSoft, Inc. This software is protected under international copyright
 * law. All use of this software is subject to MuleSoft's Master Subscription Agreement
 * (or other master license agreement) separately entered into in writing between you and
 * MuleSoft. If such an agreement is not in place, you may not use the software.
 */
package com.my.company.custom.provider.api;

import org.mule.runtime.api.component.ComponentIdentifier;
import org.mule.runtime.config.api.dsl.model.ConfigurationParameters;
import org.mule.runtime.config.api.dsl.model.ResourceProvider;
import org.mule.runtime.config.api.dsl.model.properties.ConfigurationPropertiesProvider;
import org.mule.runtime.config.api.dsl.model.properties.ConfigurationPropertiesProviderFactory;
import org.mule.runtime.config.api.dsl.model.properties.ConfigurationProperty;

import java.util.Optional;

/*import org.slf4j.Logger;
import org.slf4j.LoggerFactory;*/

import static com.my.company.custom.provider.api.SpringCloudConfigPropertiesProviderExtension.SPRING_CLOUD_CONFIG_PROPERTIES_PROVIDER;

/**
 * Builds the provider for a custom-properties-provider:config element.
 *
 * @since 1.0
 */
public class CustomConfigurationPropertiesProviderFactory implements ConfigurationPropertiesProviderFactory {

  
  //private static final Logger logger = LoggerFactory.getLogger(CustomConfigurationPropertiesProviderFactory.class);
	
  private final static String CUSTOM_PROPERTIES_PREFIX = "spring-cloud-config-getPropertyValue::";
  
  @Override
  public ComponentIdentifier getSupportedComponentIdentifier() {
    return SPRING_CLOUD_CONFIG_PROPERTIES_PROVIDER;
  }

  
  private static final String TEST_KEY = "testKey";

  
  @Override
  public ConfigurationPropertiesProvider createProvider(ConfigurationParameters parameters,
                                                        ResourceProvider externalResourceProvider) {

    // This is how you can access the configuration parameter of the <custom-properties-provider:config> element.
    String gitBranchName = parameters.getStringParameter("gitBranchName");
    String propertyFileName = parameters.getStringParameter("propertyFileName");
    String environment = parameters.getStringParameter("environment");

    return new ConfigurationPropertiesProvider() {

      @Override
      public Optional<ConfigurationProperty> getConfigurationProperty(String configurationAttributeKey) {
        // TODO change implementation to discover properties values from your custom source
        if (configurationAttributeKey.startsWith(CUSTOM_PROPERTIES_PREFIX)) {
          String effectiveKey = configurationAttributeKey.substring(CUSTOM_PROPERTIES_PREFIX.length());
          if (effectiveKey.equals(TEST_KEY)) {
            return Optional.of(new ConfigurationProperty() {

              @Override
              public Object getSource() {
                return "custom provider source";
              }

              @Override
              public Object getRawValue() {
                return gitBranchName;
              }

              @Override
              public String getKey() {
                return TEST_KEY;
              }
            });
          }
        }
        return Optional.empty();
      }

      @Override
      public String getDescription() {
        // TODO change to a meaningful name for error reporting.
        return "Spring Config properties provider";
      }
    };
  }

}
