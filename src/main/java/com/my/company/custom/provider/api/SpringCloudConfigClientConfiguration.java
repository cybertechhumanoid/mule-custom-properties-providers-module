package com.my.company.custom.provider.api;

import org.mule.runtime.extension.api.annotation.Operations;
import org.mule.runtime.extension.api.annotation.param.Parameter;

/**
 * 
 * This class represents an extension configuration, values set in this class are commonly used across multiple
 * operations since they represent something core from the extension.
 *
 */
//@Operations()
public class SpringCloudConfigClientConfiguration {
	
	@Parameter
	private String gitBranchName;
	
	@Parameter
	private String propertyFileName;
	
	@Parameter
	private String environment;

	public String getGitBranchName() {
		return gitBranchName;
	}

	public String getPropertyFileName() {
		return propertyFileName;
	}

	public String getEnvironment() {
		return environment;
	}

}
